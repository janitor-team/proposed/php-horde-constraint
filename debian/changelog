php-horde-constraint (2.0.3-9) unstable; urgency=medium

  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-5~).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 21 Jul 2020 16:34:20 +0200

php-horde-constraint (2.0.3-8) unstable; urgency=medium

  [ Mike Gabriel ]
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 10:28:56 +0200

php-horde-constraint (2.0.3-7) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959248).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/copyright: Update copyright attributions.
  * d/upstream/metadata: Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 May 2020 21:14:17 +0200

php-horde-constraint (2.0.3-6) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 07:56:18 +0200

php-horde-constraint (2.0.3-5) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Mon, 14 May 2018 23:53:14 +0200

php-horde-constraint (2.0.3-4) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 11:10:47 +0200

php-horde-constraint (2.0.3-3) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Tue, 07 Jun 2016 22:06:46 +0200

php-horde-constraint (2.0.3-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sat, 12 Mar 2016 22:31:56 +0100

php-horde-constraint (2.0.3-1) unstable; urgency=medium

  * New upstream version 2.0.3

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 13:59:50 +0100

php-horde-constraint (2.0.2-3) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 15:08:47 +0200

php-horde-constraint (2.0.2-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Aug 2015 22:14:25 +0200

php-horde-constraint (2.0.2-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.0.2

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 20:07:39 +0200

php-horde-constraint (2.0.1-7) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 11:54:04 +0200

php-horde-constraint (2.0.1-6) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 10:02:36 +0200

php-horde-constraint (2.0.1-5) unstable; urgency=medium

  * Fix DEP-8 depends

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 21:31:28 +0200

php-horde-constraint (2.0.1-4) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Sun, 24 Aug 2014 09:19:15 +0200

php-horde-constraint (2.0.1-3) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Wed, 05 Jun 2013 18:46:36 +0200

php-horde-constraint (2.0.1-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 20:28:56 +0100

php-horde-constraint (2.0.1-1) unstable; urgency=low

  * New upstream version 2.0.1

 -- Mathieu Parent <sathieu@debian.org>  Sat, 01 Dec 2012 11:06:51 +0100

php-horde-constraint (1.0.1-2) unstable; urgency=low

  * Fixed watchfile
  * Updated Standards-Version to 3.9.3: no change
  * Updated copyright format URL
  * Updated debian/pearrc to install Horde apps in /usr/share/horde
    instead of /usr/share/horde4
  * Updated Homepage
  * Added Vcs-* fields

 -- Mathieu Parent <sathieu@debian.org>  Fri, 30 Nov 2012 21:12:37 +0100

php-horde-constraint (1.0.1-1) unstable; urgency=low

  * Horde_Constraint package.
  * Initial packaging (Closes: #635786)
  * Copyright file by Soren Stoutner and Jay Barksdale

 -- Mathieu Parent <sathieu@debian.org>  Tue, 17 Jan 2012 18:44:01 +0100
